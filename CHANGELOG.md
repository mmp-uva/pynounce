# Changelog

## Version 0.1.2

* No longer support python 2.7 (since mould does not support it)
* Update to mould 0.16 

## Version 0.1.1

* Implement handler factory
* Implement filter registry

## Version 0.1.0

* First working implementation
* Support decentral message delivery through propagation
* Support central message delivery with proxies and delivery delegation
