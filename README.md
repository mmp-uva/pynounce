# PyNounce Package

This package provides a system to send messages between objects.
It can be used in centralised, decentralised or mixed structures. 

## Components

- Nodes: objects that have a radio to communicate in the system
- Radios: objects that handle communication for their owner
- Handler: object that filters and delivers messages to callbacks
- Filter: object to decide which message is delivered by the handler

### Nodes

All objects in the system that have a radio associated.


### Radios


### Handler Discovery

Radios inspect their owners on attributes with a handler associated.
This is done by searching for the "handler" attribute in every owner attribute
and then checking whether the handler is an instance of the Handler class.

When a radio discovers object attributes with an handler associated to it, it will inform the handler about this.
This allows to the handler to pass the object instance as the first parameter to the callback (i.e. self). 

### Handler

A handler takes care of the delivery of an event to a callback function.
Before an event is offered to the callback the handler determines whether it accepts the event.
If the event is accepted it is passed to callback.

The handler by default passes the event as an positional argument to the callback.
Alternatively the handler can unwrap the event parameters and pass them as keyword arguments,
in that case the handler will determine if the callback accepts an additional "_event" keyword (explicitly or implicitly).

Additional arguments can be provided at the initialisation of the handler.

Results obtained from the callback are stored in the event with the handler object as key.

### Filter

Object that contains decision logic to decide whether to accept an event.

## Message Lifetime

* Trigger -> Receive -> Process

The Process step consists of propagating events to other radios and delivering the event to known handlers:

* Process -> Propagate -> Receive -> Process
* Process -> Deliver -> Handler -> Callback

### Trigger

1. User object triggers event delivery in radio
2. The radio checks the origin of the event (sets it if missing)
3. The radio starts the receive process

### Receive / Process

1. The radio registers itself to the event as having received the event
2. The radio processes the event:
    2. Events are delivered to known handlers
    3. Events are propagated to other radios 

NOTE: Events are never propagated to the same radio twice.

## Delivery structure

The standard radio works in a decentralised way. 
It does both the propagation and delivery on a per-radio basis.
This means the radio will deliver the event to the handlers it knows 
and then propagates the event to other radios that are observing this radio.

### Propagation

Radios can register to receiving events from other radios by observing them.
When radios 

### Proxy Radios

Proxy radios allow for an alternative delivery structure by providing shortcuts in the propagation chain.
Radios are allowed to register themselves as clients of the proxy.
Incoming events to the proxy are delivered directly to each client without propagation on the client side.

* Process (Proxy) -> Deliver (Client) -> Callback

#### Delegating Radios

Delegating Radios delegate their delivery task to other radios, by removing local delivery from the receiving process. 
This disconnects delivery from propagation. This makes most sense when used with a proxy radio, as in this case the
proxy will implement delivery via its client system.

Delegating Radios are always connected bi-directional, so delegating radios propagate events to the proxy 
of which they are client.

* Trigger (delegating radio) -> Receive (delegating radio) -> Propagate (delegating radio) 
 -> Receive (proxy) -> Process (proxy)
* Process (proxy) -> Deliver (delegating radio) -> Callback

### Multi-threading

Multi-threaded radios are possible and can be implemented for all radios. The multi-threaded radio disconnects
event propagation from the running thread by placing incoming events on a queue. A pool of workers then take events
from the queue to further propagate and deliver these events.

Multi-threaded radios can be used to compartmentalise delivery networks by placing multi-threaded radios at bottlenecks.
The workload on each side of the bottleneck is shared over the pool of workers.
 
The best use for multi-threaded radios is that of the proxy radio in (semi-)centralised structures, where event delivery
to callbacks is performed by a pool of workers.

