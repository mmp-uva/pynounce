
from .base import BaseRadio

import threading
from six.moves.queue import PriorityQueue, Empty

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class ThreadedRadio(BaseRadio):

    def __init__(self, workers=(), *args, **kwargs):
        super(ThreadedRadio, self).__init__(*args, **kwargs)
        self._queue = PriorityQueue()
        # events running
        self._running_events = set()
        # workers that
        self._workers = set()
        """@type: set[pynounce.radio.async.AsyncRadioWorker]"""
        if not isinstance(workers, (list, tuple)):
            workers = [workers]
        for worker in workers:
            if isinstance(worker, ThreadedRadioWorker):
                self.register_worker(worker)

    def register_worker(self, worker):
        if not isinstance(worker, ThreadedRadioWorker):
            raise TypeError("Worker must be ThreadedRadioWorker")
        self._workers.add(worker)

    def unregister_worker(self, worker):
        self._workers.remove(worker)

    def _register_event(self, event):
        with self._lock:
            self._running_events.add(event)

    def _unregister_event(self, event):
        with self._lock:
            self._running_events.remove(event)

    def count_running(self):
        with self._lock:
            return len(self._running_events)

    def count_work(self):
        return self.count_running() + self._queue.qsize()

    def has_work(self):
        filled_queue = not self._queue.empty()
        any_running = self.count_running() > 0
        return filled_queue or any_running

    def stop(self):
        """Ask all workers to stop working"""
        for worker in self._workers:
            worker.stop()

    def terminate(self):
        """Force all workers to stop working"""
        for worker in self._workers:
            worker.terminate()

    def process(self, event):
        """Process uncouples the delivery chain to separate thread"""
        self._queue.put(event)
        return event

    def work_queue(self, block=False, timeout=None):
        """Take an event from the queue and process it"""
        try:
            # get event from queue
            event = self._queue.get(block=block, timeout=timeout)
            self._queue.task_done()
            """@type: pynounce.event.Event"""
            if not event.is_ready:
                # return event to queue
                self._queue.put(event)
            # try to deliver
            self.process_queued_event(event)
            # we evaluate cancelling right before executing
        except Empty:
            pass

    def process_queued_event(self, event):
        """The replacement"""
        self.deliver(event)
        self.propagate(event)
        return event

    def deliver(self, event):
        raise NotImplementedError

    def propagate(self, event):
        raise NotImplementedError


class ThreadedRadioWorker(threading.Thread):

    def __init__(self, owner, **kwargs):
        super(ThreadedRadioWorker, self).__init__(**kwargs)
        if not isinstance(owner, ThreadedRadio):
            raise TypeError("Owner does not support threaded radio workers")
        self._owner = owner
        # register worker to owner
        self.owner.register_worker(self)
        self.stopped = threading.Event()
        self.terminated = threading.Event()

    @property
    def owner(self):
        """ The AsyncRadio that owns this worker

        @return:
        @rtype: pynounce.radio.threaded.ThreadedRadio
        """
        return self._owner

    def stop(self):
        self.stopped.set()

    def terminate(self):
        self.terminated.set()

    @property
    def can_stop(self):
        """Can only stop if all work is done"""
        return not self.owner.has_work()

    @property
    def should_stop(self):
        """Whether the worker is asked to stop"""
        return self.stopped.is_set()

    @property
    def must_stop(self):
        """Whether the worker is forced to stop"""
        return self.terminated.is_set()

    @property
    def can_continue(self):
        """Whether the thread can continue running"""
        # if stop was requested and no events are waiting
        satisfied_stop = self.should_stop and self.can_stop
        # quit if forced to or if allowed
        return not self.must_stop and not satisfied_stop

    def run(self):
        print("Worker {} started".format(self.name))
        while self.can_continue:
            self.process()
        print("Worker {} finished".format(self.name))
        self.clean()

    def process(self):
        """Process the work"""
        # work on the queue of the owner, wait max 100 msec
        self.owner.work_queue(block=True, timeout=0.1)

    def clean(self):
        self._owner = None
