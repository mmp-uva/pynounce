
from .base import Radio
from .threaded import ThreadedRadio

import weakref

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class ProxyRadio(Radio):
    """A central radio that does the delivery task of delegating radio's

    user -> trigger -> receive -> process
    ?? process -> deliver -> receive (handler)
    process -> propagate -> receive (observer)
    process -> deliver -> queue
    queue -> deliver (client)

    OPEN ISSUES:
    - should central support delivery to it's own handlers?
    - should base implementation work with threads and queues??
    - base implementation could work without threads and queues to disconnect propagation from delivery

    """

    def __init__(self, owner=None, clients=(), **kwargs):
        super(ProxyRadio, self).__init__(owner, **kwargs)
        # register clients
        self._clients = weakref.WeakSet()
        """@type: set[pynounce.radio.Radio]"""
        if not isinstance(clients, (list, tuple)):
            clients = [clients]
        for client in clients:
            if isinstance(client, Radio):
                self._observers.add(client)

    def register_client(self, client):
        client = self.obtain_radio(client)
        with self._lock:
            self._clients.add(client)

    def unregister_client(self, client):
        with self._lock:
            self._clients.remove(client)

    def deliver(self, event):
        # should I deliver to my own handlers?
        super(ProxyRadio, self).deliver(event)
        # deliver to own clients
        self.deliver_to_clients(event)
        return event

    def deliver_to_clients(self, event):
        """ Delivers an event to all clients of this central radio

        To implement a central radio with workers, replace this with a queue

        @param event:
        @type  event: pynounce.event.Event
        @return:
        @rtype:
        """
        with self._lock:
            clients = self._clients.copy()

        for client in clients:
            self.deliver_to_client(client, event)
        return event

    def deliver_to_client(self, client, event):
        return client.deliver(event)


class ThreadedProxyRadio(ProxyRadio, ThreadedRadio):
    """Extends threading to the proxy radio"""

    pass


class DelegatingRadio(Radio):
    """Delegates it's the delivery process to another radio

    user -> trigger -> receive -> process
    process -> propagate -> receive (observer)
    # delegate to receive mechanism of proxy radio
    process -> delegate -> receive (radio)
    # consider: delegate to delivery mechanism of proxy (i.e. no propagation)
    deliver -> receive (handler)

    """

    def __init__(self, owner, proxy=None, **kwargs):
        super(DelegatingRadio, self).__init__(owner, **kwargs)
        if proxy is not None:
            self.register_proxy(proxy)

    def register_proxy(self, proxy):
        if not isinstance(proxy, Radio):
            raise TypeError("Proxy Radio must be a Radio")
        # register proxy as observer for propagation
        self.register_observer(proxy)
        # register as client to the proxy
        if isinstance(proxy, ProxyRadio):
            proxy.register_client(self)

    def unregister_proxy(self, proxy):
        # remove radio as client from proxy
        if isinstance(proxy, ProxyRadio):
            proxy.unregister_client(self)
        # remove proxy as observer
        self.unregister_observer(proxy)

    def process(self, event):
        """Only propagate"""
        return self.propagate(event)
