"""The radio implemented the event system for a node"""

from mould.core import ThreadSafeObject, CopyableObject

from pynounce.handler import HandlerFactory
from pynounce.filter import Filter, filter_factory
from pynounce.event import Event

import weakref

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class BaseRadio(ThreadSafeObject, CopyableObject):
    """Abstract Radio Interface

    Interface event handling

    user -> trigger -> receive
    receive -> process
    process -> deliver -> .... -> receive (handler)
    process -> propagate -> .... -> receive (radio)

    """

    def __init__(self, owner=None, *args, **kwargs):
        super(BaseRadio, self).__init__()
        # register owner
        self._owner = None
        self._register_owner(owner)

    def __initkwargs__(self, memo=None):
        kwargs = super(BaseRadio, self).__initkwargs__(memo=memo)
        kwargs['owner'] = self.owner
        return kwargs

    @property
    def owner(self):
        if self._owner:
            return self._owner()
        return None

    def _register_owner(self, owner):
        if owner is not None:
            self._owner = weakref.ref(owner)
        else:
            self._owner = None

    def trigger(self, event, **kwargs):
        """Receive an event for processing"""
        if isinstance(event, type) and issubclass(event, Event):
            # set owner as origin
            event = event(None, **kwargs)
        # if no origin is set, set to owner
        if event.origin is None:
            event.origin = self.owner
        # if owner is defined and origin is not equal to owner, set to owner
        elif self.owner is not None and event.origin is not self.owner:
            event.origin = self.owner
        # if origin is still None set to radio
        if event.origin is None:
            event.origin = self
        # handle event as incoming
        return self.receive(event)

    def receive(self, event):
        """Receive an event from other observer

        @param event: The event to propagate
        @type  event: pynounce.event.Event
        """
        # register radio as receiver
        event.register_radio(self)
        try:
            event = self.process(event)
        finally:
            # register radio as being finished with this event
            event.register_radio(self, True)
        return event

    def process(self, event):
        # deliver first
        self.deliver(event)
        # propagate
        self.propagate(event)
        return event

    def deliver(self, event):
        raise NotImplementedError

    def propagate(self, event):
        raise NotImplementedError


class Radio(BaseRadio):
    """Basic Radio implementation

    user -> trigger -> receive
    receive -> process
    process -> deliver -> receive (handler)
    process -> propagate -> receive (observer)

    """

    @classmethod
    def obtain_radio(cls, o, required_radio=None):
        # if no requirement is set minimally require a radio
        if required_radio is None:
            required_radio = Radio
        # use types to check object
        if not isinstance(required_radio, type):
            required_radio = type(required_radio)
        # if required radio is not a radio reject
        if not issubclass(required_radio, Radio):
            raise TypeError("Required type must be a radio")
        # if object is not a radio, try to find it
        if not isinstance(o, required_radio):
            # try to get it's radio
            if hasattr(o, "radio") and isinstance(o.radio, required_radio):
                o = o.radio
            if hasattr(o, "events") and isinstance(o.events, required_radio):
                o = o.events
        # if no radio could be found, reject
        if not isinstance(o, required_radio):
            raise TypeError("Unable to find radio in {}".format(o))
        # if everything worked return found radio
        return o

    def __init__(self, owner=None, observers=(), filters=(), ignore_from_owner=False, **kwargs):
        self._handlers = []
        self._filters = set()
        # isolate kwarg filters
        filter_kwargs = {}
        for k in list(kwargs.keys()):
            if k.startswith("filter__"):
                filter_kwargs[k] = kwargs.pop(k)
        super(Radio, self).__init__(owner, **kwargs)
        # add observers
        self._observers = weakref.WeakSet()
        """@type: set[pynounce.radio.Radio]"""
        if not isinstance(observers, (tuple, list)):
            observers = [observers]
        for observer in observers:
            if isinstance(observer, Radio):
                self._observers.add(observer)
        # load filters
        if not isinstance(filters, (tuple, list)):
            filters = [filters]
        for f in filters:
            if isinstance(f, Filter):
                self._filters.add(f)
        # create filters from kwargs
        for k, v in filter_kwargs.items():
            f = filter_factory(k.replace("filter__", ""), v)
            if isinstance(f, Filter):
                self._filters.add(f)
        self._ignore_from_owner = ignore_from_owner is True
        # consider to collect handlers directly connected to the radio?

    def _register_owner(self, owner):
        super(Radio, self)._register_owner(owner)
        if owner is not None:
            self._collect_handlers(owner)

    @property
    def propagation_filters(self):
        """A list of filters used to decide whether to propagate an event

        @rtype: list[pynounce
        """
        result = self._filters
        if len(result) == 0:
            result = [Filter()]
        return result

    @property
    def ignore_from_owner(self):
        return self._ignore_from_owner

    @ignore_from_owner.setter
    def ignore_from_owner(self, state):
        self._ignore_from_owner = state is True

    def _collect_handlers(self, owner, reset=True):
        import inspect
        if reset:
            self._handlers = []
        for name, attr in inspect.getmembers(owner, lambda a: hasattr(a, "handler")):
            hf = getattr(attr, "handler", None)
            if isinstance(hf, HandlerFactory):
                h = hf.create(attr, owner=self)
                self._handlers.append(h)

    def register_observer(self, observer):
        """Add observer """
        observer = self.obtain_radio(observer)
        self._observers.add(observer)

    def unregister_observer(self, observer):
        """Remove observer"""
        observer = self.obtain_radio(observer)
        self._observers.remove(observer)

    def deliver(self, event):
        """Deliver an event to the handlers in the radio's parent

        @param event: The event to deliver
        @type event: pynounce.event.Event
        @rtype: pynounce.event.Event
        """
        # deliver events from owner if allowed to do this or if event does not come from owner
        if not self.ignore_from_owner or event.origin is not self.owner:
            self.deliver_to_handlers(event)
        return event

    def deliver_to_handlers(self, event):
        for handler in self._handlers:
            self.deliver_to_handler(handler, event)
        return event

    def deliver_to_handler(self, handler, event):
        """Deliver the given event to the given handler

        @param handler: The handler to deliver the event to
        @type handler: pynounce.handler.Handler
        @param event: The event to deliver
        @type event: pynounce.event.Event
        """
        if not event.is_cancelled:
            handler.receive_event(event)
        return event

    def propagate(self, event):
        """Send event to observering radios

        @param event: The event to propagate
        @type event: pynounce.event.Event
        @rtype: pynounce.event.Event
        """
        if self.can_propagate(event):
            for observer in self._observers:
                if observer not in event.radios:
                    observer.receive(event)
        return event

    def can_propagate(self, event):
        """Whether this radio can propagate this event

        @rtype: bool
        """
        return all([f.test(event) for f in self.propagation_filters])
