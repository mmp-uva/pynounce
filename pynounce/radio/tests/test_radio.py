
import unittest
from pynounce.event import Event
from pynounce.handler import handle
from pynounce.radio import Radio

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class Node(object):

    def __init__(self):
        self.radio = Radio(self)

    @handle()
    def f(self, event):
        return 5

    @handle()
    def g(self, event):
        return 5 + event.params['a']


class TestRadio(unittest.TestCase):

    def test_init(self):
        subject = Node()
        # check radio initiation
        self.assertTrue(hasattr(subject, "radio"))
        # check handler decorator
        self.assertTrue(hasattr(subject.f, "handler"))
        self.assertTrue(hasattr(subject.g, "handler"))
        # check handler registration
        self.assertEqual(2, len(subject.radio._handlers))

    def test_simple_callback(self):
        subject = Node()
        # send event via radio
        e = subject.radio.trigger(Event, a=5)
        self.assertEqual(len(e.results), 2)
