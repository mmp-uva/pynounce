"""A node can send and receive events"""

from mould.core import CopyableObject
from .radio import Radio

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "BaseNode",
    "Node",
    "HierarchicalNode"
]


class BaseNode(CopyableObject):
    """The basic node implementation"""

    RADIO_CLASS = Radio

    def __init__(self, radio=None, radio_kwargs=None, *args, **kwargs):
        super(BaseNode, self).__init__(**kwargs)
        if radio is None:
            if radio_kwargs is None:
                radio_kwargs = {}
            radio = self.create_radio(**radio_kwargs)
        if not isinstance(radio, Radio):
            raise TypeError("Radio must be of type Radio")
        self.radio = radio

    # def __initkwargs__(self):
    #     import copy
    #     kwargs = super(BaseNode, self).__initkwargs__()
    #     kwargs['radio'] = copy.deepcopy(self.radio)
    #     return kwargs

    def create_radio(self, radio_class=None, *args, **kwargs):
        if radio_class is None:
            radio_class = self.RADIO_CLASS
        if not isinstance(radio_class, type) or not issubclass(radio_class, Radio):
            raise TypeError("Invalid Radio Class")
        return radio_class(self, *args, **kwargs)

    def fire_event(self, event, **kwargs):
        """Fires an event"""
        return self.radio.trigger(event, **kwargs)

    def receive_event(self, event):
        """process an event"""
        self.radio.receive(event)

    def copy(self, *args, memo=None, **kwargs):
        result = super(BaseNode, self).copy(*args, memo=memo, **kwargs)
        result._radio = self.radio.copy(owner=result)
        return result


class Node(BaseNode):

    pass


class HierarchicalNode(Node):

    def __init__(self, parent=None, **kwargs):
        self._parent = None
        super(HierarchicalNode, self).__init__(**kwargs)
        self._register_parent(parent)

    @property
    def parent(self):
        return self._parent

    @property
    def root(self):
        if isinstance(self.parent, HierarchicalNode):
            return self.parent.root
        if self.parent is not None:
            return self.parent
        return None

    def _register_parent(self, parent):
        self._unregister_parent()
        self._parent = parent
        self._attach_parent_radio()

    def _attach_parent_radio(self):
        if hasattr(self.parent, "radio"):
            if isinstance(self.parent.radio, Radio):
                self.radio.register_observer(self.parent.radio)

    def _unregister_parent(self):
        self._deattach_parent_radio()

    def _deattach_parent_radio(self):
        if hasattr(self.parent, "radio"):
            if isinstance(self.parent.radio, Radio):
                self.radio.unregister_observer(self.parent.radio)

