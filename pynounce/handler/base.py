"""Handles event messages"""

from mould.core import CopyableObject
from pynounce.filter import Filter, filter_factory

import inspect
import types

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "BaseHandler",
    "Handler",
    "HandlerFactory",
    "handle", "handle_unwrap"
]


def get(d, name, default=None, pop=False):
    """Utility method to retrieve elements from a dictionary"""
    if name in d.keys():
        return d.pop(name) if pop else d.get(name)
    return default


class BaseHandler(CopyableObject):
    """BaseHandlers are the endpoints that handle events.

    Events are offered to every handler found but are prevented from being handled by filters.
    """

    def __init__(self, filters=None, **kwargs):
        self._filters = set()
        # isolate kwarg filters
        filter_kwargs = {}
        for k in list(kwargs.keys()):
            if k.startswith("filter__"):
                filter_kwargs[k] = kwargs.pop(k)
        super(BaseHandler, self).__init__(**kwargs)
        # save filters
        if filters is None:
            filters = []
        if isinstance(filters, (set, tuple)):
            filters = list(filters)
        if not isinstance(filters, list):
            filters = [filters]
        for f in filters:
            if isinstance(f, Filter):
                self._filters.add(f)
        # set properties
        self.weight = get(kwargs, "weight", 0, pop=True)
        # create filters from kwargs
        for k, v in filter_kwargs.items():
            f = filter_factory(k.replace("filter__", ""), v)
            if isinstance(f, Filter):
                self._filters.add(f)

    def __initkwargs__(self, memo=None):
        import copy
        kwargs = super(BaseHandler, self).__initkwargs__(memo=memo)
        kwargs['filters'] = copy.deepcopy(self.filters)
        return kwargs

    @property
    def filters(self):
        """The Filters attached to this handler

        :rtype: list[pynounce.filter.Filter]
        """
        return self._filters

    def match(self, event):
        """Determine if the message is accepted by the filters used in this handler

        :rtype: bool
        """
        return all([f.test(event) for f in self.filters])

    def receive_event(self, event):
        """Process Event

        :return: Whether the message was received succesfully.
        :rtype: bool
        """
        if self.match(event):
            return self.handle(event)
        return event

    def handle(self, event):
        pass    # overload


class Handler(BaseHandler):
    """A Handler delegates the handling of the event to a callback"""

    def __init__(self, callback=None, *, f_args=(), f_kwargs=None, owner=None, unwrap=False, **kwargs):
        super(Handler, self).__init__(**kwargs)
        self._callback = None
        # register the callback
        if isinstance(callback, (types.MethodType, types.FunctionType)):
            self.register_callback(callback)
        # register owner
        self._owner = None
        self.register_owner(owner)
        # whether to unwrap the event arguments
        self.unwrap = unwrap
        # use the handlers args and kwargs to always pass certain info
        # this is useful so that nodes can pass themselves as the self argument
        # events do not have control over the args (they only accept kwargs)
        # event kwargs take precedence over handler kwargs
        self.args = list(f_args)
        if f_kwargs is None:
            f_kwargs = {}
        self.kwargs = f_kwargs

    def __initkwargs__(self, memo=None):
        import copy
        kwargs = super(Handler, self).__initkwargs__(memo=memo)
        kwargs['callback'] = self._callback
        kwargs['f_args'] = copy.copy(self.args)
        kwargs['owner'] = self.owner
        kwargs['unwrap'] = self.unwrap
        return kwargs

    def register_callback(self, callback):
        """Register the callback to the handler and vice versa"""
        self.unregister_callback()
        self._callback = callback

    def unregister_callback(self):
        """Remove the callback"""
        self._callback = None

    @property
    def owner(self):
        return self._owner

    def register_owner(self, owner):
        self._owner = owner

    def unregister_owner(self):
        self._owner = None

    def handle(self, event):
        """Handle an event"""
        callback = self._callback
        if not isinstance(callback, (types.MethodType, types.FunctionType)):
            raise ValueError("Unable to handle events without a callback attached")

        args = self.args[:]
        has_self = False
        kwargs = self.kwargs.copy()
        spec = inspect.getfullargspec(callback)

        # add instance
        if 'self' in spec.args:
            if getattr(callback, "__self__", None) is not None:
                has_self = True
            elif self.owner is not None:
                args.insert(0, self.owner)

        if self.unwrap:
            # include event in call if **kwargs are accepted or _event is a named argument
            if "_event" in spec.args or "_event" in spec.kwonlyargs or spec.varkw is not None:
                kwargs["_event"] = event
            kwargs.update(event.params)
        elif len(args) < (len(spec.args) - has_self):
            index = len(args)
            if "_event" in spec.args:
                index = spec.args.index("_event")
            args.insert(index, event)

        try:
            result = callback(*args, **kwargs)
            event.register_result(self, result)
        except TypeError as e:
            raise Warning("Unable to deliver event: {}".format(e))
        finally:
            return event

    def __call__(self, f):
        self.register_callback(f)
        return f


class HandlerFactory(object):

    def __init__(self, handler=None, *args, callback=None, **kwargs):
        if handler is None:
            handler = Handler
        if not isinstance(handler, type):
            handler = type(handler)
        if not issubclass(handler, Handler):
            raise TypeError("Given handler must inherit from Handler")
        self._callback = callback
        self._handler = handler
        self._args = args
        self._kwargs = kwargs

    @classmethod
    def register(cls, callback, handler=None, *args, **kwargs):
        setattr(callback, "handler", cls(handler, callback=callback, *args, **kwargs))
        return callback

    def create(self, callback=None, *args, **kwargs):
        if callback is None:
            callback = self._callback
        nargs = list(self._args)[:]
        nkwargs = self._kwargs.copy()
        # args replace arguments in nargs by position
        for i, arg in enumerate(args):
            if i < len(nargs):
                nargs.insert(i, arg)
            else:
                nargs.append(arg)
        # kwargs replace arguments in nkwargs by key
        nkwargs.update(kwargs)
        return self._handler(callback, *nargs, **nkwargs)

    def __call__(self, f):
        return self.register(f, *self._args, **self._kwargs)


def handle(**kwargs):
    """Decorator to register an event handler to a function"""
    # this will create the handler and register it to f in the handler attribute
    def decorator(f):
        HandlerFactory.register(f, **kwargs)
        return f
    return decorator


def handle_unwrap(**kwargs):
    """Decorator to automatically unwrap an event into the function arguments

    The function can get access to the event by accepting keyword arguments (**kwargs) or
        by accepting the '_event' argument.
    """
    return handle(unwrap=True, **kwargs)
