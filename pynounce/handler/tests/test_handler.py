
import unittest
from pynounce.event import Event
from pynounce.handler import BaseHandler, Handler, HandlerFactory, handle, handle_unwrap

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class TestBaseHandler(unittest.TestCase):

    subject_class = BaseHandler

    def test_init(self):
        pass


class TestFunctionHandler(TestBaseHandler):

    subject_class = Handler

    class A(object):

        def f(self, event):
            return event.params['a']

    class B(A):

        pass

    def test_simple(self):

        def f():
            return 5

        h = self.subject_class(f)
        e = Event(self)
        result = h.receive_event(e)
        self.assertEqual(e, result)
        self.assertIn(5, e.results.values())

        def f(event):
            return 5

        h = self.subject_class(f)
        e = Event(self)
        result = h.receive_event(e)
        self.assertEqual(e, result)
        self.assertIn(5, e.results.values())

    def test_unwrap(self):

        def f(a):
            return a

        h = self.subject_class(f, unwrap=True)
        e = Event(self, a=5)
        result = h.receive_event(e)
        self.assertEqual(e, result)
        self.assertIn(5, e.results.values())

        # test missing argument
        e = Event(self)
        h.receive_event(e)
        self.assertEqual(0, len(list(e.results.values())))
        self.assertNotIn(5, e.results.values())

    def test_event_access(self):

        def f(a, _event):
            return a + _event.params['a']

        h = self.subject_class(f, unwrap=True)
        e = Event(self, a=5)
        result = h.receive_event(e)
        # now the event added 5 to 5 so 10
        self.assertEqual(e, result)
        self.assertIn(10, e.results.values())

    def test_object_handler(self):
        a = self.A()
        h = self.subject_class(a.f, owner=a)
        e = Event(self, a=5)
        h.receive_event(e)
        self.assertIn(5, e.results.values())


class TestHandlerFactory(unittest.TestCase):

    subject_class = HandlerFactory

    def test_object(self):
        class B(object):

            @self.subject_class()
            def f(self):
                return 3

        b = B()
        self.assertTrue(hasattr(b.f, "handler"))
        h = b.f.handler.create()
        h.register_owner(b)
        e = Event(self, a=5)
        h.receive_event(e)
        self.assertIn(3, e.results.values())

        class A(object):

            @self.subject_class()
            def f(self, event):
                return event.params['a']

        a = A()
        self.assertTrue(hasattr(a.f, "handler"))
        h = a.f.handler.create()
        h.register_owner(a)
        e = Event(self, a=5)
        h.receive_event(e)
        self.assertIn(5, e.results.values())

    def test_decorator(self):
        class A(object):

            @handle()
            def f(self, event):
                return event.params['a']

        a = A()
        self.assertTrue(hasattr(a.f, "handler"))

    def test_decorator_unwrap(self):
        class A(object):

            @handle_unwrap()
            def f(self, event):
                return event.params['a']

        a = A()
        self.assertTrue(hasattr(a.f, "handler"))

    def test_inheritance(self):
        class A(object):

            @self.subject_class()
            def f(self, event):
                return event.params['a']

        class B(A):

            pass

        a = A()
        self.assertTrue(hasattr(a.f, "handler"))
        b = B()
        self.assertTrue(hasattr(b.f, "handler"))
