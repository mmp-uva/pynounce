
from mould.tests import SubjectTestCase
from pynounce.event import Event
from pynounce.filter import *
from pynounce.filter.registry import FilterRegistry

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class A(Event):

    pass


class TestFilterRegistry(SubjectTestCase):

    subject_class = FilterRegistry

    def create_subject(self):
        return self.subject_class(require_descendant=Filter, allow_instance=False)

    def test_registry_entry_points(self):
        """Test whether the registry fills itself with entry points"""
        # fill with entries
        self.subject.load_entry_points("pynounce.filters")
        # expect PropertyFilter to be in there
        self.assertIn(PropertyFilter.dot_path(), self.subject.list())


class TestFilter(SubjectTestCase):

    subject_class = Filter

    def __init__(self, methodName='runTest'):
        super(TestFilter, self).__init__(methodName=methodName)
        self._subject = None

    @property
    def subject(self):
        """ Subject of the test

        :rtype: pynounce.filter.Filter
        """
        return self._subject

    def test_filter(self):
        e = Event(self)
        self.assertTrue(self.subject.test(e))
        e = A(self)
        self.assertTrue(self.subject.test(e))

    def test_filter_negate(self):
        self.subject.negate = True
        e = Event(self)
        self.assertFalse(self.subject.test(e))
        e = A(self)
        self.assertFalse(self.subject.test(e))


class TestPropertyFilter(TestFilter):

    subject_class = PropertyFilter

    def create_subject(self):
        return self.subject_class("foo", "a")

    def test_filter(self):
        e = Event(self)
        self.assertFalse(self.subject.test(e))
        e = Event(self, foo="a")
        self.assertTrue(self.subject.test(e))
        e = Event(self, bar="b")
        self.assertFalse(self.subject.test(e))

    def test_filter_negate(self):
        self.subject.negate = True
        e = Event(self)
        self.assertTrue(self.subject.test(e))
        e = Event(self, foo="a")
        self.assertFalse(self.subject.test(e))
        e = Event(self, bar="b")
        self.assertTrue(self.subject.test(e))


class TestTopicFilter(TestPropertyFilter):

    subject_class = TopicFilter

    def create_subject(self):
        return self.subject_class("a")

    def test_filter(self):
        e = Event(self)
        self.assertFalse(self.subject.test(e))
        e = Event(self, topic="a")
        self.assertTrue(self.subject.test(e))
        e = Event(self, topic="b")
        self.assertFalse(self.subject.test(e))

    def test_filter_negate(self):
        self.subject.negate = True
        e = Event(self)
        self.assertTrue(self.subject.test(e))
        e = Event(self, topic="a")
        self.assertFalse(self.subject.test(e))
        e = Event(self, topic="b")
        self.assertTrue(self.subject.test(e))


class TestTypeFilter(TestFilter):

    subject_class = TypeFilter

    def create_subject(self):
        return self.subject_class(A)

    def test_filter(self):
        e = Event(self)
        self.assertFalse(self.subject.test(e))
        e = A(self)
        self.assertTrue(self.subject.test(e))

    def test_filter_negate(self):
        self.subject.negate = True
        e = Event(self)
        self.assertTrue(self.subject.test(e))
        e = A(self)
        self.assertFalse(self.subject.test(e))


class TestOriginFilter(TestFilter):

    subject_class = OriginFilter

    def create_subject(self):
        return self.subject_class(self)

    def test_filter(self):
        e = Event(self)
        self.assertTrue(self.subject.test(e))
        origin = Event(self)
        e = Event(origin)
        self.assertFalse(self.subject.test(e))

    def test_filter_negate(self):
        self.subject.negate = True
        e = Event(self)
        self.assertFalse(self.subject.test(e))
        origin = Event(self)
        e = A(origin)
        self.assertTrue(self.subject.test(e))


class TestOriginTypeFilter(TestFilter):

    subject_class = OriginTypeFilter

    def create_subject(self):
        return self.subject_class(A)

    def test_filter(self):
        origin = A(self)
        e = Event(origin)
        self.assertTrue(self.subject.test(e))
        origin = Event(self)
        e = Event(origin)
        self.assertFalse(self.subject.test(e))

    def test_filter_negate(self):
        self.subject.negate = True
        origin = A(self)
        e = Event(origin)
        self.assertFalse(self.subject.test(e))
        origin = Event(self)
        e = Event(origin)
        self.assertTrue(self.subject.test(e))
