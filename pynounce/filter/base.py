"""Filters message"""

from mould.core import CopyableObject
from pynounce.event import Event

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    # "load_filters",
    "filter_factory",

    "Filter",
    "PropertyFilter",
    "TopicFilter",
    "TypeFilter",
    "OriginFilter", "OriginTypeFilter",
]


def load_filters():
    results = [
        PropertyFilter,
        TopicFilter,
        TypeFilter,
        OriginFilter,
        OriginTypeFilter
    ]
    return results


def filter_factory(name, value):
    """Creates a filter from a name value combination"""
    result = None
    # find filer by name
    from pynounce.filter.registry import filters
    for result in filters.retrieve(name, algorithms="name", partial=False):
        break
    if isinstance(result, type) and issubclass(result, Filter):
        return result(value)
    return None


class Filter(CopyableObject):
    """Basic filter which accepts all events"""

    name = None

    def __init__(self, negate=False, **kwargs):
        super(Filter, self).__init__(**kwargs)
        self._negate = negate is True
        self.name = getattr(self, "name", None) or type(self).__name__

    @property
    def negate(self):
        """Whether the filter will negate the output of accepts(event)"""
        return self._negate

    @negate.setter
    def negate(self, state):
        self._negate = state is True

    def test(self, event):
        """Tests whether the event is accepted by the filter

        @note: Do NOT override this method, it uses accepts(event) to get the correct output.
        @param event: The event to test
        @type  event: pynounce.event.Event
        @return: Whether the filter accepts/rejects the event (depends on self.negate)
        @rtype: bool
        """
        if self.negate:
            return self.rejects(event)
        return self.accepts(event)

    def accepts(self, event):
        """Does the filter accept this event?

        @note: Override this method to implement custom event filter.
        @param event: The event to test acceptance for.
        @type event: pynounce.event.Event
        @return: Whether the filter accepts this event
        @rtype: bool
        """
        return True

    def rejects(self, event):
        """Does the filter reject this event?

        @note: This method simply negates the output of accepts(event).
        @param event: The event to test rejection for.
        @type event: pynounce.event.Event
        @return: Whether the filter rejects this event
        @rtype: bool
        """
        return not self.accepts(event)


class PropertyFilter(Filter):
    """Filter events by a property (if present)"""

    name = "property"

    def __init__(self, name, value, **kwargs):
        super(PropertyFilter, self).__init__(**kwargs)
        self.property = name
        self.value = value

    def __initargs__(self, memo=None):
        args = super(PropertyFilter, self).__initargs__(memo=memo)

        return args + (self.property, self.value)

    def accepts(self, event):
        try:
            return getattr(event, self.property) == self.value
        except AttributeError:
            return False


class TopicFilter(PropertyFilter):
    """Filter events by topic"""

    def __init__(self, value, **kwargs):
        if 'name' in kwargs.keys():
            kwargs.pop('name')
        super(TopicFilter, self).__init__(name='topic', value=value, **kwargs)


class TypeFilter(Filter):
    """Filter events by desired message type(s)"""

    name = "type"

    def __init__(self, required_type, **kwargs):
        super(TypeFilter, self).__init__(**kwargs)
        if not (isinstance(required_type, type) and issubclass(required_type, Event)):
            raise TypeError("Required type must inherit from Event")
        self._required_type = required_type

    def __initargs__(self, memo=None):
        args = super(TypeFilter, self).__initargs__(memo=memo)

        return args + (self.required_type, )

    @property
    def required_type(self):
        return self._required_type

    def accepts(self, message):
        return isinstance(message, self.required_type)


class OriginFilter(Filter):
    """Filter events by origin instance"""

    name = "origin"

    def __init__(self, required_origin, **kwargs):
        super(OriginFilter, self).__init__(**kwargs)
        self._required_origin = required_origin

    def __initargs__(self, memo=None):
        args = super(OriginFilter, self).__initargs__(memo=memo)

        return args + (self.required_origin, )

    @property
    def required_origin(self):
        return self._required_origin

    def accepts(self, event):
        if isinstance(self.required_origin, (list, tuple)):
            return event.origin in self.required_origin
        return event.origin == self.required_origin


class OriginTypeFilter(Filter):
    """Filter events by origin type"""

    name = "origin_type"

    def __init__(self, required_origin, **kwargs):
        super(OriginTypeFilter, self).__init__(**kwargs)
        # only accept classes
        if not isinstance(required_origin, type):
            required_origin = type(required_origin)
        self._required_origin = required_origin

    def __initargs__(self, memo=None):
        args = super(OriginTypeFilter, self).__initargs__(memo=memo)

        return args + (self.required_origin, )

    @property
    def required_origin(self):
        return self._required_origin

    def accepts(self, event):
        return isinstance(event.origin, self.required_origin)
