"""General Filters"""

from .base import *

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "filter_factory",

    "Filter",
    "PropertyFilter",
    "TopicFilter",
    "TypeFilter",
    "OriginFilter",
    "OriginTypeFilter"
]
