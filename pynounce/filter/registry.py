
from .base import Filter
from mould.utilities.registry import Registry, matches


class FilterRegistry(Registry):

    @matches("name")
    def match_name(self, name, partial=True, **kwargs):
        if not isinstance(name, str):
            return  # cannot match
        for k, v in self._registry.items():
            match_partial = partial and (
                    name.startswith(v.name) or name.endswith(v.name) or
                    v.name.startswith(name) or v.name.endswith(name)
            )
            if v.name == name or match_partial:
                yield k


filters = FilterRegistry(
    require_descendant=Filter, allow_instance=False
).load_entry_points("pynounce.filters")
"""Registry of filter classes, populated with entry points"""
