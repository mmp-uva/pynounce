
from mould.tests import SubjectTestCase
from pynounce.event import Event
from pynounce.event import *

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class A(Event):

    pass


class TestEvent(SubjectTestCase):

    subject_class = Event
